<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Category;
use App\User;
use App\Article;
use App\Comment;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth'], function(){
    //category
    Route::get('/category', 'CategoryController@index');
    Route::post('/category', 'CategoryController@store');
    Route::get('/category/{category}', function (Category $category) {
        return $category;
    });
    Route::put('/category/{id}', 'CategoryController@update');
    Route::delete('/category/{id}', 'CategoryController@destroy');

    //user
    Route::get('/user', 'UserController@index');
    Route::post('/user', 'UserController@store');
    Route::get('/user/{user}', function (User $user) {
        return $user;
    });
    Route::put('/user/{id}', 'UserController@update');
    Route::delete('/user/{id}', 'UserController@destroy');

    //article
    Route::get('/article', 'ArticleController@index');
    Route::post('/article', 'ArticleController@store');
    // Route::get('/article/{article}', function (Article $article) {
    //     return $article;
    // });
    Route::get('/article/{id}', 'ArticleController@show');
    Route::put('/article/{id}', 'ArticleController@update');
    Route::delete('/article/{id}', 'ArticleController@destroy');

    //comment
    Route::get('/comment', 'CommentController@index');
    Route::post('/comment', 'CommentController@store');
    Route::get('/comment/{comment}', function (Comment $comment) {
        return $comment;
    });
    Route::put('/comment/{id}', 'CommentController@update');
    Route::delete('/comment/{id}', 'CommentController@destroy');
});

// Route::apiResource('category', 'CategoryController');
// Route::apiResource('user', 'UserController');
// Route::apiResource('article', 'ArticleController');
// Route::apiResource('comment', 'CommentController');

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});
