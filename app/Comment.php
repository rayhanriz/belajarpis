<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $primaryKey = 'id';
    protected $fillable = ['content','user_id','article_id'];

    public function User(){
    	return $this->belongsTo('App\User','user_id','id');
    }

    public function Article(){
    	return $this->belongsTo('App\Article','article_id','id');
    }
}
