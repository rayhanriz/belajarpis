<?php

namespace App\Http\Controllers;
use App\Article;
use Illuminate\Http\Request;
use App\Http\Requests\StoreArticle;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ar=Article::all();
        return response()->json(['message' => 'OK', 'data' => $ar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArticle $request)
    {
        $validated = $request->validated();

        $create = Article::create([
            'title'=>$request->title,
            'content'=>$request->content,
            'user_id'=>$request->user_id,
            'category_id'=>$request->category_id
        ]);
        return response()->json(['message' => 'OK', 'data' => $create]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = Article::with(['comment'=>function($q){
            $q->orderby('created_at','desc');}])
            ->findOrFail($id);
            return response()->json(['message' => 'OK', 'data' => $show]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreArticle $request, $id)
    {
        $validated = $request->validated();

        $update = Article::find($id);
        $form_data = array(
            'title'=>$request->title,
            'content'=>$request->content,
            'user_id'=>$request->user_id,
            'category_id'=>$request->category_id
        );
        $update->update($form_data);
        return response()->json(['message' => 'OK', 'data' => $update]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Article::findOrFail($id);
        $delete->delete();
        return response()->json(['message' => 'OK', 'data' => $delete]);
    }
}
