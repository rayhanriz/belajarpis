<?php

namespace App\Http\Controllers;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Requests\StoreComment;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $com=Comment::all();
        return response()->json(['message' => 'OK', 'data' => $com]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComment $request)
    {
        $validated = $request->validated();

        $create = Comment::create([
            'content'=>$request->content,
            'user_id'=>$request->user_id,
            'article_id'=>$request->article_id
        ]);
        return response()->json(['message' => 'OK', 'data' => $create]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = Comment::find($id);
        return response()->json(['message' => 'OK', 'data' => $show]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreComment $request, $id)
    {
        $validated = $request->validated();

        $update = Comment::find($id);
        $form_data = array(
            'content'=>$request->content,
            'user_id'=>$request->user_id,
            'article_id'=>$request->article_id
        );
        $update->update($form_data);
        return response()->json(['message' => 'OK', 'data' => $update]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Comment::findOrFail($id);
        $delete->delete();
        return response()->json(['message' => 'OK', 'data' => $delete]);
    }
}
