<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';
    protected $primaryKey = 'id';
    protected $fillable = ['title','content','user_id','category_id'];

    public function Category(){
    	return $this->belongsTo('App\Category','category_id','id');
    }
    public function User(){
    	return $this->belongsTo('App\User','user_id','id');
    }
    public function Comment(){
    	return $this->hasMany('App\Comment');
    }
}
